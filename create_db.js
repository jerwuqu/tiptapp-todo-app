const sqlite3 = require('sqlite3').verbose();

// Open database
const db = new sqlite3.Database('todo-app.db', (err) => {
    if (err) {
        console.error(err);
        process.exit(1);
    }

    // Create todo list table
    db.run('CREATE TABLE `tasks` (' +
        '`id`         INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,' +
        '`title`      TEXT NOT NULL,' +
        '`done`       INTEGER DEFAULT 0,' +
        '`deleted`    INTEGER DEFAULT 0' +
    ');');

    // Create event log table
    db.run('CREATE TABLE `events` (' +
        '`id`         INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,' +
        '`event`      TEXT NOT NULL,' +
        '`task_id`    INTEGER NOT NULL' +
    ');');

    // Close database
    db.close((err) => {
        if (err) {
            return console.error(err.message);
        }
    });
});
