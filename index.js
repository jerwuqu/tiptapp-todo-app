const Hapi    = require('hapi');
const sqlite3 = require('sqlite3');

// Create Hapi server object
const server = Hapi.server({
    port: 8080,
    host: '0.0.0.0'
});

const initHapi = async () => {
    await server.start();
    console.log(`Server running at: ${server.info.uri}`);
};

// Open SQLite database
const db = new sqlite3.Database('todo-app.db', (err) => {
    if (err) {
        console.error(err);
        process.exit(1);
    }

    // Start web server
    initHapi();
});

// Endpoint for getting all tasks in the todo list
server.route({
    method: 'GET',
    path:   '/list',
    handler: (request, h) => {
        return new Promise((resolve, reject) => {
            // Fetch all tasks in the todo list
            db.all('SELECT * FROM `tasks` ORDER BY `id`', [], function(err, rows) {
                if (err) {
                    console.error(err);
                    reject();
                    return;
                }

                resolve({
                    success: true,
                    tasks:   rows
                });
            });
        });
    }
});

// Helper function for logging events to database
const logListEvent = (event, taskId) => {
    db.run('INSERT INTO `events`(`event`, `task_id`) VALUES(?, ?)', [event, taskId], function(err) {
        if (err) {
            console.error(err);
        }
    });
};

// Endpoint for getting all events for the todo list
server.route({
    method: 'GET',
    path:   '/list/events',
    handler: (request, h) => {
        return new Promise((resolve, reject) => {
            // Fetch all events for the todo list
            db.all('SELECT `event`, `task_id` FROM `events` ORDER BY `id`', [], function(err, rows) {
                if (err) {
                    console.error(err);
                    reject();
                    return;
                }

                resolve({
                    success: true,
                    events:  rows
                });
            });
        });
    }
});

// Endpoint for adding a new task
server.route({
    method: 'POST',
    path:   '/list',
    handler: (request, h) => {
        // Basic sanity checking
        if (!request.payload || !request.payload.title || typeof request.payload.title !== 'string') {
            return {
                success: false,
                message: 'No task title!'
            };
        } else if (request.payload.title.length > 256) {
            return {
                success: false,
                message: 'Task title too long!'
            };
        }

        return new Promise((resolve, reject) => {
            // Insert new task with given title
            db.run('INSERT INTO `tasks`(`title`) VALUES(?)', [request.payload.title], function(err) {
                if (err) {
                    console.error(err);
                    reject();
                    return;
                }

                // Push event if something changed
                if (this.changes > 0) {
                    logListEvent('add', this.lastID);
                }

                resolve({success: true});
            });
        });
    }
});

// Endpoint for deleting a task
server.route({
    method: 'DELETE',
    path:   '/task/{id}',
    handler: (request, h) => {
        const taskId = parseInt(request.params.id);

        return new Promise((resolve, reject) => {
            // Mark task with given ID as deleted
            db.run('UPDATE `tasks` SET `deleted` = 1 WHERE `id` = ? AND `deleted` = 0', [taskId], function(err) {
                if (err) {
                    console.error(err);
                    reject();
                    return;
                }

                // Push event if something changed
                if (this.changes > 0) {
                    logListEvent('delete', taskId);
                }

                resolve({success: true});
            });
        });
    }
});

// Helper function for marking and unmarking tasks as done
const taskMarkDone = (taskId, isDone) => {
    return new Promise((resolve, reject) => {
        // Mark task with given ID as done depending on isDone
        db.run('UPDATE `tasks` SET `done` = ? WHERE `id` = ? AND `done` = ?', [isDone, taskId, !isDone], function(err) {
            if (err) {
                console.error(err);
                reject();
                return;
            }

            // Push event if something changed
            if (this.changes > 0) {
                logListEvent(isDone ? 'done' : 'undo', taskId);
            }

            resolve({success: true});
        });
    });
};

// Endpoint for marking a task as done
server.route({
    method: 'PUT',
    path:   '/task/{id}/done',
    handler: (request, h) => {
        return taskMarkDone(parseInt(request.params.id), 1);
    }
});

// Endpoint for marking a task as undone
server.route({
    method: 'PUT',
    path:   '/task/{id}/undo',
    handler: (request, h) => {
        return taskMarkDone(parseInt(request.params.id), 0);
    }
});
